from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('userregister', views.userregister, name='userregister'),
	path('change_userpassword', views.change_userpassword, name="change_userpassword"),
	path('userlogin', views.userlogin_view, name="userlogin"),
	path('userlogout', views.userlogout_view, name="userlogout"),
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
]