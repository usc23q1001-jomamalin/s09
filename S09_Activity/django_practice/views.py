from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
from .models import GroceryItem

# Create your views here.
def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {'groceryitem_list': groceryitem_list}
	return render(request, "django_practice/index.html", context)

def groceryitem(request, groceryitem_id):
	response = "You are viewing the details of %s"
	return HttpResponse(response % groceryitem_id)

def userregister(request):
	users = User.objects.all()

	is_user_registered = False
	context = {
		"is_user_registered": is_user_registered
	}

	for indiv_user in users:
		if indiv_user.username == "John Doe":
			is_user_registered = True
			break

	if is_user_registered == False:
		user = User()
		user.username = "John Doe"
		user.first_name = "John"
		user.last_name = "Doe"
		user.email = "john@mail.com"
		user.set_password("john1234")
		user.is_staff = False
		user.is_active = True
		user.save()
		context = {
			"first_name": user.first_name,
			"last_name": user.last_name
		}

	return render(request, "django_practice/userregister.html", context)

def change_userpassword(request):
	is_user_authenticated = False

	user = authenticate(username="John Doe", password="john1234")
	print(user)

	if user is not None:
		authenticated_user = User.objects.get(username="John Doe")
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True

	context = {
		"is_user_authenticated": is_user_authenticated
	}

	return render(request, "django_practice/change_userpassword.html", context)

def userlogin_view(request):
	username = "John Doe"
	password = "johndoe1"
	user = authenticate(username=username, password=password)
	print(user)

	if user is not None:
		login(request, user)
		return redirect("index")

	else:
		is_user_authenticated = False
		context = {
			"is_user_authenticated": is_user_authenticated
		}
		
		return render(request, "django_practice/userlogin.html", context)

def userlogout_view(request):
	logout(request)
	return redirect("index")